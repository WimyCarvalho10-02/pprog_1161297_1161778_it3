/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import utilitarios.Data;

/**
 *
 * @author pedroterleira
 */
public class Evento {
        
    private String titulo;
    private String txtDesc;
    private Data[] periodo;
    private ListaOrganizadores listaOrganizadores;
    private ListaFAEs listaFAEs;
    private ListaCandidaturas listaCandidaturas;

    public Evento(String titulo, String txtDesc, Data[] periodo, ListaOrganizadores listaOrganizadores) {
        this.titulo = titulo;
        this.txtDesc = txtDesc;
        this.periodo = periodo;
        this.listaOrganizadores = listaOrganizadores;
    }
    
    public FAE getFAE(Utilizador u){
        return listaFAEs.getFAE(u);
    }
    
    public List<Atribuicao> getListaAtribuicoesPorDecidir(FAE fae){
        return listaCandidaturas.getListaAtribuicoesPorDecidir(fae);
    }
    
    public ListaCandidaturas getListaCandidaturas() {
        return listaCandidaturas;
    }
    
    public boolean isOrganizador(Organizador o) {
        return listaOrganizadores.isOrganizador(o);
    }

    boolean isFAE(FAE fae) {
        return listaFAEs.isFAE(fae);
    }

    public void registarAtribuicoes(List<Atribuicao> listaAtribuicoes) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
