/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pedroterleira
 */
public class ListaEventos {
    
    private List<Evento> listaEventos;

    public ListaEventos() {
    }

    public List<Evento> getListaEventos() {
        return listaEventos;
    }
    
    public List<Evento> getListaEventos(Organizador o){
        List<Evento> listaEventosOrganizador = new ArrayList<>();
        for(Evento e : getListaEventos()){
            if(e.isOrganizador(o)){
                listaEventosOrganizador.add(e);
            }
        }
        return listaEventosOrganizador;
    }
    
    public List<Evento> getListaEventos(FAE fae){
        List<Evento> listaEventosFAE = new ArrayList<>();
        for(Evento e : getListaEventos()){
            if(e.isFAE(fae)){
                listaEventosFAE.add(e);
            }
        }
        return listaEventosFAE;
    }
}
