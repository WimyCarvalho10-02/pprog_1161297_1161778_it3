/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author pedroterleira
 */
public class Representante {
    
    private Utilizador utilizador;
    private String nomeEmpresa;

    public Representante(Utilizador utilizador) {
        this.utilizador = utilizador;
    }
    
    public String getNomeEmpresa(){
        return nomeEmpresa;
    }
    
}
