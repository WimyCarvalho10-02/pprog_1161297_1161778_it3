/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author pedroterleira
 */
public class Atribuicao {

    private Candidatura c;
    private FAE fae;
    private Decisao decisao;

    public Atribuicao() {
    }

    public void setFAE(FAE fae) {
        this.fae = fae;
    }

    public void setDecisao(Decisao decisao) {
        this.decisao = decisao;
    }
    
    public FAE getFAE(){
        return fae;
    }
    
    public Candidatura getCandidatura(){
        return c;
    }
    
    public Decisao novaDecisao(){
        return new Decisao();
    }

    public boolean valida(Decisao d) {
        return c.valida(d);
    }

}
