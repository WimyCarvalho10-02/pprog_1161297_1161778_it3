package model;

import java.util.ArrayList;
import java.util.List;

public class ListaCandidaturas {

    private List<Candidatura> listaCandidaturas = new ArrayList<>();

    public ListaCandidaturas() {
    }

    public List<Candidatura> getListaCandidaturas() {
        return listaCandidaturas;
    }

    public List<Atribuicao> getListaAtribuicoesPorDecidir(FAE fae) {
        List<Atribuicao> listaAtribuicoes = new ArrayList<>();
        for (Candidatura c : getListaCandidaturas()) {
            for (Atribuicao atrib : c.getListaAtribuicoes().getListaAtribuicoes()) {
                if (atrib.getFAE().equals(fae)) {
                    listaAtribuicoes.add(atrib);
                }
            }
        }
        return listaAtribuicoes;
    }

    public Candidatura novaCandidatura(String dados) {
        Candidatura c = new Candidatura();
        c.setDados(dados);
        return c;
    }

    public boolean valida(Candidatura c) {
        return c.valida();
    }

    public boolean registaCandidatura(Candidatura c) {
        if (valida(c)) {
            return listaCandidaturas.add(c);
        } else {
            return false;
        }
    }
}
