package model;

import java.util.ArrayList;
import java.util.List;

public class ListaOrganizadores {

    private List<Organizador> listaOrganizadores = new ArrayList<>();

    public ListaOrganizadores() {
    }

    public List<Organizador> getListaOrganizadores() {
        return listaOrganizadores;
    }

    boolean isOrganizador(Organizador o) {
        boolean flag = false;
        for(Organizador org : getListaOrganizadores()){
            if(org.equals(o)){
                flag = true;
            }
        }
        return flag;
    }
}
