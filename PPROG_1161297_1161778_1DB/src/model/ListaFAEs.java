package model;

import java.util.ArrayList;
import java.util.List;

public class ListaFAEs {

    private List<FAE> listaFAEs = new ArrayList<>();

    public ListaFAEs() {
    }

    public List<FAE> getListaFAEs() {
        return listaFAEs;
    }

    public FAE getFAE(Utilizador u) {
        FAE f = null;
        for (FAE fae : getListaFAEs()) {
            if (fae.getUtilizador().equals(u)) {
                f = fae;
            }
        }
        return f;
    }

    public boolean isFAE(FAE fae) {
        boolean flag = false;
        for(FAE f : getListaFAEs()){
            if(f.equals(fae)){
                flag = true;
            }
        }
        return flag;
    }

}
