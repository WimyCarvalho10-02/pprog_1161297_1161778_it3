/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author pedroterleira
 */
public class Decisao {
    
    private Boolean decisao;
    private String textDesc;
    
    public Decisao(){
    }

    public void setDecisao(Boolean decisao) {
        this.decisao = decisao;
    }

    public void setTextDesc(String textDesc) {
        this.textDesc = textDesc;
    }
    
}
