/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Formatter;

/**
 *
 * @author pedroterleira
 */
public class FicheiroListaCandidaturas {

    public static final String NOME_FICHEIRO_BINARIO = "CandidaturasAceites.bin";
    public static final String NOME_FICHEIRO_TEXTO = "CandidaturasAceites.txt";

    public FicheiroListaCandidaturas() {
    }

    public ListaCandidaturas[] ler(String nomeFicheiro) {
        ListaCandidaturas[] listaCandidaturas = new ListaCandidaturas[2];
        try {
            ObjectInputStream in = new ObjectInputStream(
                    new FileInputStream(nomeFicheiro));
            try {
                listaCandidaturas[0] = (ListaCandidaturas) in.readObject();
                listaCandidaturas[1] = (ListaCandidaturas) in.readObject();
            } finally {
                in.close();
            }
            return listaCandidaturas;
        } catch (IOException | ClassNotFoundException ex) {
            return null;
        }
    }

    public boolean guardar(String nomeFicheiro, ListaCandidaturas[] listas) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(
                    new FileOutputStream(nomeFicheiro));
            try {
                out.writeObject(listas[0]);
                out.writeObject(listas[1]);
            } finally {
                out.close();
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    public boolean criarFicheiroTexto(ListaCandidaturas listaCandidaturas) {
        try {
            File ficheiro = new File(FicheiroListaCandidaturas.NOME_FICHEIRO_TEXTO);
            Formatter out = new Formatter(ficheiro);
            try {
                listaCandidaturas.ordenarPorPosicao();
                out.format("Candidaturas Aceites: %n%n");
                for (int i = 0; i < listaCandidaturas.tamanho(); i++) {
                    out.format("%s%n", listaCandidaturas.obterCandidatura(i));
                }
                return true;
            } finally {
                out.close();
            }
        } catch (FileNotFoundException ex) {
            return false;
        }
    }
}
