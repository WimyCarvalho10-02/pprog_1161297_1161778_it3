package model;

import java.util.ArrayList;
import java.util.List;

public class ListaUtilizadores {

    private List<Utilizador> listaUtilizadores = new ArrayList<>();

    public ListaUtilizadores() {
    }

    public List<Utilizador> getListaUtilizadores() {
        return this.listaUtilizadores;
    }
    /*

    public void setListaUtilizadores(List<Utilizador> listaUtilizadores) {
        this.listaUtilizadores = listaUtilizadores;
    }

    public void addUtilizador(Utilizador u) {
        this.listaUtilizadores.add(u);
    }

    public Utilizador getUtilizador(String userID, String pass) {
        for (Utilizador u : this.listaUtilizadores) {
            if ((u.getUsername().equals(userID) || u.getEmail().equals(userID)) && u.getPass().equals(pass)) {
                return u;
            }
        }
        return null;
    }

    public void removerUtilizador(Utilizador u) {
        this.listaUtilizadores.remove(u);
    }

    public List<String> getListaNomesUtilizadores() {
        List<String> listaNomesUtilizadores = new ArrayList<>();
        for (Utilizador u : this.listaUtilizadores) {
            listaNomesUtilizadores.add(u.getNome());
        }
        return listaNomesUtilizadores;
    }

    public List<String> getListaNomesRepresentantes() {
        List<String> listaNomesRepresentantes = new ArrayList<>();
        for (Utilizador u : this.listaUtilizadores) {
            if (u instanceof Representante) {
                listaNomesRepresentantes.add(u.getNome());
            }
        }
        return listaNomesRepresentantes;
    }

    public boolean validaUtilizador(String utilizadorID) {
        for (Utilizador u : this.listaUtilizadores) {
            if (utilizadorID.equals(u.getUsername()) || utilizadorID.equals(u.getEmail())) {
                return true;
            }
        }
        return false;
    }

*/
}
