/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.MainMenuController;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import model.FicheiroListaCandidaturas;
import model.ListaCandidaturas;

/**
 *
 * @author berccar
 */
public class MainMenuUI extends JFrame {

    private MainMenuController controller;
    private MainMenuUI framePai;
    private PainelListaCandidaturas pListaCandidaturas;

    private static final int JANELA_LARGURA = 300;

    private static final int JANELA_ALTURA = 200;

    public MainMenuUI(/*MainMenuController controller*/) {
        super("Main Menu");

        //this.controller = controller;
        setLayout(new BorderLayout());

        criarComponentes();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(JANELA_LARGURA, JANELA_ALTURA);
        setVisible(true);
    }

    private void criarComponentes() {

        JPanel pUCs = criarPainelUCs();
        JPanel pButtons = criarPainelButton();

        add(pUCs, BorderLayout.CENTER);
        add(pButtons, BorderLayout.SOUTH);
        add(new JPanel(), BorderLayout.NORTH);
        add(new JPanel(), BorderLayout.EAST);
        add(new JPanel(), BorderLayout.WEST);
    }

    private JPanel criarPainelUCs() {
        JPanel p = new JPanel();

        p.setLayout(new GridLayout(3, 1, 0, 10));

        JButton btnUC3 = criarButtonUC3();
        JButton btnUC4 = criarButtonUC4();
        JButton btnUC5 = criarButtonUC5();

        p.add(btnUC3);
        p.add(btnUC4);
        p.add(btnUC5);

        return p;
    }

    private JButton criarBtnStandard(String nome, int k, String toolTipText) {
        JButton btn = new JButton(nome);
        final int BOTAO_LARGURA = 150, BOTAO_ALTURA = 30;
        btn.setPreferredSize(new Dimension(BOTAO_LARGURA, BOTAO_ALTURA));
        btn.setMnemonic(k);
        btn.setToolTipText(toolTipText);
        btn.setFont(new Font("Arial", Font.PLAIN, 13));

        return btn;
    }

    private JButton criarButtonUC3() {
        JButton btn = criarBtnStandard("Atribuir Candidatura", KeyEvent.VK_A, "Atribui candidaturas as FAEs");

        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new UC3UI();
            }
        });

        return btn;
    }

    private JButton criarButtonUC4() {
        JButton btn = criarBtnStandard("Decidir Candidatura", KeyEvent.VK_D, "Decidir sobre candidatura");

        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FicheiroListaCandidaturas ficheiroListaCandidaturas = new FicheiroListaCandidaturas();

                ListaCandidaturas[] listas = ficheiroListaCandidaturas
                        .ler(ficheiroListaCandidaturas.NOME_FICHEIRO_BINARIO);

                ListaCandidaturas listaCompleta;
                ListaCandidaturas listaAceites;
                if (listas == null) {
                    listaCompleta = new ListaCandidaturas();
                    listaAceites = new ListaCandidaturas();
                } else {
                    listaCompleta = listas[0];
                    listaAceites = listas[1];
                }

                new UC4UI(listaCompleta, listaAceites, ficheiroListaCandidaturas);
            }
        });

        return btn;
    }

    private JButton criarButtonUC5() {
        JButton btn = criarBtnStandard("Submeter Candidatura", KeyEvent.VK_S, "Submete candidatura para avaliação");

        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new UC5UI(framePai);
            }
        });

        return btn;
    }

    private JPanel criarPainelButton() {
        JPanel p = new JPanel();

        p.setLayout(new FlowLayout());

        JButton btnSair = criarButtonSair();

        p.add(btnSair);

        return p;
    }

    private JButton criarButtonSair() {
        JButton btn = criarBtnStandard("Sair", KeyEvent.VK_S, "Sair do programa");

        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        return btn;
    }
    
    public JPanel getPainelLista() {
        return pListaCandidaturas;
    }

}
