/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

/**
 *
 * @author pedroterleira
 */

import java.awt.BorderLayout;
import javax.swing.*;
import model.ListaCandidaturas;


public class PainelListaCandidaturas extends JPanel {
    private JList lstListaCandidaturas; 
    private ListaCandidaturas listaCandidaturas; 

    public PainelListaCandidaturas(ListaCandidaturas listaCandidaturas) {
        super();
        
        this.listaCandidaturas = listaCandidaturas;

        setLayout(new BorderLayout());
        
        add(criarPainelListaCandidaturas(), BorderLayout.CENTER);
    }
    
    public JList getLstCandidaturas() {
        return lstListaCandidaturas;
    }

    private JScrollPane criarPainelListaCandidaturas() {

        ModeloListaCandidaturas modeloListaCandidaturas = 
                new ModeloListaCandidaturas(listaCandidaturas);
        
        lstListaCandidaturas = new JList(modeloListaCandidaturas);
                
        JScrollPane scrollPane = new JScrollPane(lstListaCandidaturas); 

        final int MARGEM_SUPERIOR = 20, MARGEM_INFERIOR = 20;
        final int MARGEM_ESQUERDA = 20, MARGEM_DIREITA = 20;
        scrollPane.setBorder(BorderFactory.createEmptyBorder( MARGEM_SUPERIOR, 
                                                              MARGEM_ESQUERDA,
                                                              MARGEM_INFERIOR, 
                                                              MARGEM_DIREITA));

        return scrollPane;
    }
}
