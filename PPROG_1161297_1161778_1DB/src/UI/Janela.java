/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import static java.awt.Frame.MAXIMIZED_BOTH;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import model.CentroEventos;
import model.FAE;
import model.FicheiroCentroEventos;
import model.Organizador;
import model.Representante;
import model.Utilizador;

/**
 *
 * @author pedroterleira
 */
public class Janela extends JFrame {

    private Organizador organizador;
    private Utilizador utilizador;
    
    private CentroEventos centroEventos;
    private FicheiroCentroEventos ficheiroCentroEventos;
    private JPanel pAccao;

    public Janela(CentroEventos centroEventos,
            FicheiroCentroEventos ficheiroCentroEventos, Organizador organizador, FAE fae, Representante representante) {

        super("Centro de Eventos");

        this.centroEventos = centroEventos;
        this.ficheiroCentroEventos = ficheiroCentroEventos;
        this.organizador = organizador;
        this.utilizador = fae;
        this.representante = representante;

        criarComponentes();

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                terminar("Fechar");
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null);
        setExtendedState(MAXIMIZED_BOTH);
        setVisible(true);
    }

    private void criarComponentes() {

        JMenuBar menuBar = criarBarraMenus();
        setJMenuBar(menuBar);

        pAccao = criarPanelUCs();
        add(pAccao, BorderLayout.CENTER);

    }

    private JMenuBar criarBarraMenus() {
        JMenuBar menuBar = new JMenuBar();

        menuBar.add(criarMenuCentroEventos());
        menuBar.add(criarMenuOpcoes());

        return menuBar;
    }

    private JMenu criarMenuCentroEventos() {
        JMenu menu = new JMenu("CentroEventos");
        menu.setMnemonic(KeyEvent.VK_D);

        menu.addMenuListener(new MenuListener() {
            @Override
            public void menuSelected(MenuEvent e) {
            }

            @Override
            public void menuDeselected(MenuEvent e) {
            }

            @Override
            public void menuCanceled(MenuEvent e) {
            }
        });

        menu.add(criarSubMenuDados());
        menu.addSeparator();
        menu.add(criarItemSair());

        return menu;
    }

    private JMenu criarMenuOpcoes() {
        JMenu menu = new JMenu("Opções");
        menu.setMnemonic(KeyEvent.VK_O);

        menu.add(criarSubMenuEstilo());
        menu.addSeparator();
        menu.add(criarItemAcerca());

        return menu;
    }

    private JMenu criarSubMenuDados() {
        JMenu menu = new JMenu("Dados");
        menu.setMnemonic(KeyEvent.VK_D);

        menu.add(criarItemImportarDados());
        menu.add(criarItemExportarDados());

        return menu;
    }

    private JMenuItem criarItemImportarDados() {
        JMenuItem item = new JMenuItem("Importar", KeyEvent.VK_I);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_MASK));
        item.setEnabled(false);

        return item;
    } //Incompleto

    private JMenuItem criarItemExportarDados() {
        JMenuItem item = new JMenuItem("Exportar", KeyEvent.VK_X);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_MASK));
        item.setEnabled(false);

        return item;
    } //Incompleto

    private JMenu criarSubMenuEstilo() {
        JMenu menu = new JMenu("Estilo");
        menu.setMnemonic(KeyEvent.VK_E);

        for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
            menu.add(criarItemEstilo(info));
        }

        return menu;
    }

    private JMenuItem criarItemEstilo(UIManager.LookAndFeelInfo info) {
        JMenuItem item = new JMenuItem(info.getName());

        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JMenuItem menuItem = (JMenuItem) e.getSource();
                try {
                    for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                        if (menuItem.getActionCommand().equals(info.getName())) {
                            UIManager.setLookAndFeel(info.getClassName());
                            break;
                        }
                    }
                    SwingUtilities.updateComponentTreeUI(Janela.this);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(
                            Janela.this,
                            ex.getMessage(),
                            "Estilo " + menuItem.getActionCommand(),
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        return item;
    }

    private JMenuItem criarItemSair() {
        JMenuItem item = new JMenuItem("Sair", KeyEvent.VK_S);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.ALT_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                terminar("Fechar");
            }
        });

        return item;
    }

    private JMenuItem criarItemAcerca() {
        JMenuItem item = new JMenuItem("Acerca", KeyEvent.VK_A);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(
                        Janela.this,
                        "@Copyright\nTrabalho Pratico 3\nPPROG 2016/2017",
                        "Acerca",
                        JOptionPane.INFORMATION_MESSAGE);
            }
        });
        return item;
    }

    private void terminar(String tituloDialogo) {
        guardarLista(tituloDialogo);
        dispose();
    }

    private void guardarLista(String tituloDialogo) {
        boolean listaGuardada = true/*ficheiroCentroEventos.guardar(centroEventos)*/;
        if (!listaGuardada) {
            JOptionPane.showMessageDialog(Janela.this,
                    "Impossível guardar lista!",
                    tituloDialogo,
                    JOptionPane.ERROR_MESSAGE);
        }
    }//Incompleto

    private JPanel criarPanelUCs() {
        JPanel p = new JPanel();

        p.setLayout(new GridLayout(3, 1, 0, 10));

        JButton btnUC3 = criarButtonUC3();
        JButton btnUC4 = criarButtonUC4();
        JButton btnUC5 = criarButtonUC5();

        p.add(btnUC3);
        p.add(btnUC4);
        p.add(btnUC5);

        return p;
    }

    private JButton criarBtnStandard(String nome, int k, String toolTipText) {
        JButton btn = new JButton(nome);
        final int BOTAO_LARGURA = 150, BOTAO_ALTURA = 30;
        btn.setPreferredSize(new Dimension(BOTAO_LARGURA, BOTAO_ALTURA));
        btn.setMnemonic(k);
        btn.setToolTipText(toolTipText);
        btn.setFont(new Font("Arial", Font.PLAIN, 13));

        return btn;
    }

    private JButton criarButtonUC3() {
        JButton btn = criarBtnStandard("Atribuir Candidatura", KeyEvent.VK_A, "Atribui candidaturas as FAEs");

        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pAccao = new UC3UI(centroEventos, organizador);
            }
        });

        return btn;
    }

    private JButton criarButtonUC4() {
        JButton btn = criarBtnStandard("Decidir Candidatura", KeyEvent.VK_D, "Decidir sobre candidatura");

        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pAccao = new UC4UI(centroEventos, utilizador);
            }
        });

        return btn;
    }

    private JButton criarButtonUC5() {
        JButton btn = criarBtnStandard("Submeter Candidatura", KeyEvent.VK_S, "Submete candidatura para avaliação");

        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pAccao = new UC5UI(centroEventos);
            }
        });

        return btn;
    }
}
