/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.UC4Controller;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import model.Atribuicao;
import model.CentroEventos;
import model.Evento;
import model.Utilizador;

/**
 *
 * @author pedroterleira
 */
public class UC4UI extends JPanel {

    private UC4Controller controller;

    private JPanel pMae;
    private JComboBox jcb;
    private JTextField txtDesc;

    public UC4UI(CentroEventos centroEventos, Utilizador utilizador) {

        controller = new UC4Controller(centroEventos, utilizador);

        final int NUMERO_LINHAS = 5, NUMERO_COLUNAS = 1;
        GridLayout gl = new GridLayout(NUMERO_LINHAS, NUMERO_COLUNAS);
        setLayout(gl);

        criarComponentes();

        setVisible(true);

    }

    private void criarComponentes() {
        pMae = criarPainelMae();
        add(pMae);
    }

    private JPanel criarPainelMae() {
        JPanel p = new JPanel();

        Vector comboBoxItems = new Vector(controller.getListaEventosFAE());
        final DefaultComboBoxModel model = new DefaultComboBoxModel(comboBoxItems);
        jcb = new JComboBox(model);

        JButton btn = criarButtonEscolherEvento();

        p.add(jcb);
        p.add(btn);

        return p;
    }

    private JButton criarButtonEscolherEvento() {
        JButton btn = new JButton("Escolher");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.escolheEvento((Evento) jcb.getSelectedItem());
                pMae = criarPainelCandidaturas();
            }
        });
        return btn;
    }

    private JPanel criarPainelCandidaturas() {
        JPanel p = new JPanel();

        Vector comboBoxItems = new Vector(controller.getListaAtribuicoesPorDecidir());
        final DefaultComboBoxModel model = new DefaultComboBoxModel(comboBoxItems);
        jcb = new JComboBox(model);

        JButton btn = criarButtonEscolherAtribuicao();

        p.add(jcb);

        return p;
    }

    private JButton criarButtonEscolherAtribuicao() {
        JButton btn = new JButton("Escolher");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pMae = criarPainelConfirmacao(controller.getInfoCandidatura((Atribuicao) jcb.getSelectedItem()));
            }
        });
        return btn;
    }

    private JPanel criarPainelConfirmacao(String s) {
        JPanel p = new JPanel();

        JLabel lbl1 = new JLabel(s);

        txtDesc = criarText();

        JButton btn1 = criarButtonAceitar();
        JButton btn2 = criarButtonRecusar();

        p.add(lbl1);
        p.add(txtDesc);
        p.add(btn1);
        p.add(btn2);

        return p;
    }

    private JButton criarButtonAceitar() {
        JButton btn = new JButton("Aceitar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.setDecisao(true, txtDesc.getText());
                pMae = criarPainelRegistar();
            }
        });
        return btn;
    }

    private JButton criarButtonRecusar() {
        JButton btn = new JButton("Recusar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.setDecisao(false, txtDesc.getText());
                pMae = criarPainelRegistar();
            }
        });
        return btn;
    }

    private JTextField criarText() {
        JTextField jtf = new JTextField();
        return jtf;
    }

    private JPanel criarPainelRegistar() {
        JPanel p = new JPanel();

        JLabel lbl1 = new JLabel(txtDesc.getText());

        JButton btn = criarButtonRegistar();

        p.add(lbl1);
        p.add(btn);

        return p;
    }

    private JButton criarButtonRegistar() {
        JButton btn = new JButton("Registar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.registaDecisao();
            }
        });
        return btn;
    }
}
