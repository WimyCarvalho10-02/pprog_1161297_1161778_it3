/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.UC3Controller;
import Controller.UC5Controller;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.border.EmptyBorder;
import model.Candidatura;
import model.CentroEventos;
import model.Evento;
import model.ListaCandidaturas;

/**
 *
 * @author pedroterleira
 */
public class UC5UI extends JPanel {

    private UC5Controller controller;

    private JPanel pMae;
    private JComboBox jcb;
    private JTextField txtTitular, txtNumero, txtArea, txtMorada;
    private ListaCandidaturas listaCandidaturas;
    private JList JListaCandidaturas;

    private static final Dimension LABEL_TAMANHO = new JLabel("Telefone:").getPreferredSize();

    public UC5UI(CentroEventos centroEventos) {
        
        controller = new UC5Controller(centroEventos);  

        final int NUMERO_LINHAS = 5, NUMERO_COLUNAS = 1;
        GridLayout gl = new GridLayout(NUMERO_LINHAS, NUMERO_COLUNAS);
        setLayout(gl);

        criarComponentes();

        setVisible(true);
    }

    private void criarComponentes() {
        pMae = criarPainelMae();
        
        add(pMae);
    }

    private JPanel criarPainelNome() {
        JLabel lbl = new JLabel("Nome:");
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 15;
        txtTitular = new JTextField(CAMPO_LARGURA);
        txtTitular.requestFocus();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtTitular);

        return p;
    }

    private JPanel criarPainelArea() {
        JLabel lbl = new JLabel("Área:");
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 15;
        txtArea = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(lbl);
        p.add(txtArea);

        return p;
    }

    private JPanel criarPainelNumero() {
        JLabel lbl = new JLabel("Telefone:");
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 15;
        txtNumero = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(lbl);
        p.add(txtNumero);

        return p;
    }

    private JPanel criarPainelMorada() {
        JLabel lbl = new JLabel("Morada:");
        lbl.setPreferredSize(LABEL_TAMANHO);

        final int CAMPO_LARGURA = 15;
        txtMorada = new JTextField(CAMPO_LARGURA);
        txtMorada.requestFocus();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtMorada);

        return p;
    }

    private JPanel criarPainelBotoes() {
        JButton btnOK = criarBotaoOK2();
        getRootPane().setDefaultButton(btnOK);

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOK);

        return p;
    }

    private JButton criarBotaoOK2() {
        JButton btn = new JButton("Submeter");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String dados = txtTitular.getText()+txtArea.getText()+txtMorada.getText()+txtNumero.getText();                    
                    controller.setDadosCandidatura(dados);
                    pMae=criarPainelConfirmacao();

                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, 
                            "Tem que introduzir um número inteiro com 9 digitos.",
                            "Novo Telefone",
                            JOptionPane.WARNING_MESSAGE);
                    txtNumero.requestFocusInWindow();
                } catch (IllegalArgumentException ex) {
                    JOptionPane.showMessageDialog(null,
                            ex.getMessage(),
                            "Novo Telefone",
                            JOptionPane.WARNING_MESSAGE);
                    txtTitular.requestFocusInWindow();
                }
            }
        });
        return btn;
    }

    private JPanel criarPainelDados() {
        JPanel p = new JPanel();
        
        JPanel p1 = criarPainelNome();
        JPanel p2 = criarPainelMorada();
        JPanel p3 = criarPainelNumero();
        JPanel p4 = criarPainelArea();
        JPanel p5 = criarPainelBotoes();

        p.add(p1);
        p.add(p2);
        p.add(p3);
        p.add(p4);
        p.add(p5);
        
        return p;
    }

    private JPanel criarPainelMae() {
        JPanel p = new JPanel();
        
        Vector comboBoxItems = new Vector(controller.getEventos());
        final DefaultComboBoxModel model = new DefaultComboBoxModel(comboBoxItems);
        jcb = new JComboBox(model);
        
        JButton btn = criarButtonOK1();
        
        p.add(jcb);
        p.add(btn);
        
        return p;
    }

    private JButton criarButtonOK1() {
        JButton btn = new JButton("Escolher");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.setEvento((Evento)jcb.getSelectedItem());
                pMae =criarPainelDados();
            }
        });
        return btn;
    }
    
    private JPanel criarPainelConfirmacao(){
        JPanel p = new JPanel();
        
        JLabel lbl1 = new JLabel("Nome: "+txtTitular.getText());
        JLabel lbl2 = new JLabel("Numero: "+txtNumero.getText());
        JLabel lbl3 = new JLabel("Area: "+txtArea.getText());
        JLabel lbl4 = new JLabel("Morada: "+txtMorada.getText());
        
        JButton btn = criarButtonConfirmar();
        
        p.add(lbl1);
        p.add(lbl2);
        p.add(lbl3);
        p.add(lbl4);
        p.add(btn);
        
        return p;
    }

    private JButton criarButtonConfirmar() {
        JButton btn = new JButton("Confirmar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.registaCandidatura();
            }
        });
        return btn;
    }
}
