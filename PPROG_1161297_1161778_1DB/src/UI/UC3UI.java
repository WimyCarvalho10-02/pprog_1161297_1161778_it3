/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.UC3Controller;
import com.sun.glass.events.KeyEvent;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import model.Candidatura;
import model.CentroEventos;
import model.Evento;
import model.FAE;
import model.MecanismoAtribuicao;
import model.Organizador;

/**
 *
 * @author pedroterleira
 */
public class UC3UI extends JPanel {

    private UC3Controller controller;
    
    private JPanel pPrincipal;
    private JPanel pButtons;
    private JComboBox jcb;
    private Vector comboBoxItems;

    private static final int H_GAP = 0;
    private static final int V_GAP = 15;

    public UC3UI(CentroEventos centroEventos, Organizador organizador) {

        controller= new UC3Controller(centroEventos, organizador);
        
        
        setLayout(new BorderLayout(H_GAP, V_GAP));

        criarComponentes();

        setVisible(true);
    }

    private void criarComponentes() {
        pPrincipal = criarPainelJcbEscolha();
        pButtons = criarPainelBtnEscolha();

        add(pPrincipal, BorderLayout.CENTER);
        add(pButtons, BorderLayout.SOUTH);
        add(new JPanel(), BorderLayout.NORTH);
        add(new JPanel(), BorderLayout.WEST);
        add(new JPanel(), BorderLayout.EAST);
        
    }

    private JPanel criarPainelJcbEscolha() {
        JPanel p = new JPanel();
        p.setLayout(new GridLayout(2, 1));
        JComboBox jcbEventos = criarJcbEventos();
        JComboBox jcbMecanismos = criarJcbMecanismos();
        
        p.add(jcbEventos);
        p.add(jcbMecanismos);
        
        return p;
    }
    
    private JComboBox criarJcbEventos() {
        Vector comboBoxItems = new Vector(controller.getListaEventos());
        final DefaultComboBoxModel model = new DefaultComboBoxModel(comboBoxItems);
        jcb = new JComboBox(model);
        
        return jcb;
    }
    
    private JComboBox criarJcbMecanismos() {
        final DefaultComboBoxModel model = new DefaultComboBoxModel(comboBoxItems);
        JComboBox jcb = new JComboBox(model);
        
        return jcb;
    }
    
    private JPanel criarPainelBtnEscolha(){
        JPanel p = new JPanel();
        
        p.setLayout(new FlowLayout(FlowLayout.CENTER, 30, 0));
        
        JButton btnSeguinte = criarBtnSeguinte();
        
        p.add(btnSeguinte);
        
        return p;
    }

    private JButton criarBtnSeguinte() {
        JButton btn = criarBtnStandard("Seguinte", KeyEvent.VK_S, "Gera atribuições de candidaturas");
        
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                comboBoxItems = new Vector(controller.setEvento((Evento)jcb.getSelectedItem()));
                controller.setEvento((Evento)jcb.getSelectedItem());
                pPrincipal = criarPainelLblMostrar();
                pButtons = criarPainelBtnMostrar();
            }
        });
        
        return btn;
    }

    private JPanel criarPainelLblMostrar() {
        JPanel p = new JPanel();
        p.setLayout(new GridLayout(UC3Controller.getListaAtribuicoes().size(), 2));
        for (Object[] atribuicao : UC3Controller.getListaAtribuicoes()) {
            p.add(criarLabelAtribuicaoCandidatura(atribuicao));
            p.add(criarLabelAtribuicaoFAE(atribuicao));
        }
        return p;
    }

    private JLabel criarLabelAtribuicaoCandidatura(Object[] atribuicao) {
        JLabel lbl = new JLabel();
        lbl.setText(((Candidatura) atribuicao[0]).getRepresentante().getNomeEmpresa() + ": ");

        return lbl;
    }

    private JLabel criarLabelAtribuicaoFAE(Object[] atribuicao) {
        JLabel lbl = new JLabel();
        String nomesFAE = ((ArrayList<FAE>) atribuicao[1]).get(1).getUtilizador().getNome();
        for (int i = 1; i < ((ArrayList<FAE>) atribuicao[1]).size(); i++) {
            nomesFAE += ((ArrayList<FAE>) atribuicao[1]).get(i).getUtilizador().getNome();
        }
        lbl.setText(nomesFAE);

        return lbl;
    }

    private JPanel criarPainelBtnMostrar(){
        JPanel p = new JPanel();
        
        p.setLayout(new FlowLayout(FlowLayout.CENTER, 30, 0));
        
        JButton btnVoltar = criarBtnVoltar();
        JButton btnGuardar = criarBtnGuardar();
        
        p.add(btnVoltar);
        p.add(btnGuardar);
        
        return p;
    }
    
    private JButton criarBtnVoltar() {
        JButton btn = criarBtnStandard("Voltar", KeyEvent.VK_V, "Voltar a escolher Evento/Mecanismo");
        
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        
        return btn;
    }

    private JButton criarBtnGuardar() {
        JButton btn = criarBtnStandard("Guardar", KeyEvent.VK_G, "Guarda as atribuições geradas");
        
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        
        return btn;
    }
    
    private JButton criarBtnStandard(String nome, int k, String toolTipText) {
        JButton btn = new JButton(nome);
        final int BOTAO_LARGURA = 150, BOTAO_ALTURA = 30;
        btn.setPreferredSize(new Dimension(BOTAO_LARGURA, BOTAO_ALTURA));
        btn.setMnemonic(k);
        btn.setToolTipText(toolTipText);
        btn.setFont(new Font("Arial", Font.PLAIN, 13));

        return btn;
    }

}
