/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import javax.swing.AbstractListModel;
import model.Candidatura;
import model.ListaCandidaturas;

/**
 *
 * @author pedroterleira
 */
public class ModeloListaCandidaturas extends AbstractListModel {
    
    private ListaCandidaturas listaCandidaturas;
    
    public ModeloListaCandidaturas(ListaCandidaturas listaCandidaturas) {
        this.listaCandidaturas = listaCandidaturas;
    }

    @Override
    public int getSize() {
        return listaCandidaturas.tamanho();
    }

    @Override
    public Object getElementAt(int indice) {
        return listaCandidaturas.obterCandidatura(indice);
    }
    
    public ListaCandidaturas getListaCandidaturas(){
        return listaCandidaturas;
    }
    
    public boolean addElement(Candidatura candidatura){
        boolean candidaturaAdicionada = listaCandidaturas.adicionarCandidatura(candidatura);
        if(candidaturaAdicionada)
            fireIntervalAdded(this, getSize()-1, getSize()-1);
        return candidaturaAdicionada;
    } 

    public boolean removeElement(Candidatura candidatura){
        int indice = listaCandidaturas.indiceDe(candidatura);
        boolean candidaturaRemovida = listaCandidaturas.removerCandidatura(candidatura);
        if(candidaturaRemovida)
            fireIntervalRemoved(this, indice, indice);
        return candidaturaRemovida;
    }
    
    public boolean contains(Candidatura candidatura){
        return listaCandidaturas.contem(candidatura);
    }
    
    public void sort(){
        listaCandidaturas.ordenarPorPosicao();
    }
    
}
