/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.util.List;
import model.Algoritmo;
import model.Atribuicao;
import model.CentroEventos;
import model.Evento;
import model.ListaEventos;
import model.ListaAlgoritmos;
import model.Organizador;

/**
 *
 * @author pedroterleira
 */
public class UC3Controller {

    private static CentroEventos ce;
    private static Organizador o;
    private static Evento e;
    private static List<Atribuicao> listaAtribuicoes;

    public UC3Controller(CentroEventos centroEventos, Organizador o) {
        ce = centroEventos;
        this.o = o;
    }

    public static List<Evento> getListaEventos() {
        ListaEventos le = ce.getListaEventos();
        return le.getListaEventos(o);
    }

    public static List<Algoritmo> setEvento(Evento ev) {
        e = ev;
        ListaAlgoritmos la = ce.getListaAlgoritmos();
        return la.getAlgoritmos();
    }

    public static void executarAlgoritmo(Algoritmo alg) {
        listaAtribuicoes = alg.criaDistribuicao(e);
    }

    public static void registarDistribuicao() {
        e.registarAtribuicoes(listaAtribuicoes);
    }

}
