/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.util.List;
import model.Atribuicao;
import model.Candidatura;
import model.CentroEventos;
import model.Decisao;
import model.Evento;
import model.FAE;
import model.Utilizador;

/**
 *
 * @author pedroterleira
 */
public class UC4Controller {

    private static CentroEventos ce;
    private static Utilizador u;
    private static Evento e;
    private static Atribuicao atrib;
    private static Decisao d;

    public UC4Controller(CentroEventos centroEventos, Utilizador utilizador) {
        ce = centroEventos;
        u = utilizador;
    }

    public static List<Evento> getListaEventosFAE() {
        return ce.getListaEventos().getListaEventos(new FAE(u));
    }

    public static void escolheEvento(Evento ev) {
        e = ev;
    }

    public static List<Atribuicao> getListaAtribuicoesPorDecidir() {
        return e.getListaAtribuicoesPorDecidir(new FAE(u));
    }

    public static String getInfoCandidatura(Atribuicao atribc) {
        atrib = atribc;
        return atribc.getCandidatura().getInfoCandidatura();
    }

    public static boolean setDecisao(Boolean decisao, String txtDesc) {
        Decisao dc = atrib.novaDecisao();
        dc.setDecisao(decisao);
        dc.setTextDesc(txtDesc);
        d = dc;
        return atrib.valida(dc);
    }

    public static void registaDecisao() {
        atrib.setDecisao(d);
    }

}
