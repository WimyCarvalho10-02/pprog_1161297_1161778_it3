/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.util.List;
import model.Candidatura;
import model.CentroEventos;
import model.Evento;
import model.ListaCandidaturas;
import model.ListaEventos;

/**
 *
 * @author pedroterleira
 */
public class UC5Controller {

    private static CentroEventos ce;
    private static ListaEventos listaEventos;
    private static Evento e;
    private static ListaCandidaturas listaCandidaturas;
    private static Candidatura c;
    

    public UC5Controller(CentroEventos centroEventos) {
        ce = centroEventos;
    }

    public static List<Evento> getEventos() {
        listaEventos = ce.getListaEventos();
        return listaEventos.getListaEventos();
    }

    public static void setEvento(Evento ev) {
        e = ev;
    }
    
    public static boolean setDadosCandidatura(String dados){
        listaCandidaturas = e.getListaCandidaturas();
        c = listaCandidaturas.novaCandidatura(dados);
        return listaCandidaturas.valida(c);
    }
    
    public static boolean registaCandidatura(){
        return listaCandidaturas.registaCandidatura(c);
    }

}
